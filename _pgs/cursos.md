---
title: Cursos
layout: page
image: /assets/img/plataformacursos.png
---

## Cursos - aprofundamento, conhecimento e formação
---

Após uma primeira edição do curso, virtual em virtude do contexto de pandemia, que contou com mais de 150 participantes,

sob o nome _A Revolução Cubana: Sessenta anos depois_ 

O coletivo atualmente organiza o curso:

*Desafios históricos e conjunturais da Revolução Cubana*
