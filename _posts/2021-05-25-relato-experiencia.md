---
title: Relato de Experiência sobre o Curso Revolução Cubana - 60 anos depois
date: 2020-11-27T18:29:21.302Z
image: /assets/uploads/2021-relato-de-experiencia.png
author: maitehernandez
tags: []
---
Como forma de maior difusão aos resultados excepcionais do curso, membros do coletivo prepararam, submeteram e tiveram aprovado relato de experiência do curso que contou com mais de 100 participantes.
