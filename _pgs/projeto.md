---
title: O projeto
layout: page
image: /assets/uploads/cropped-esoview1.jpg
---
## O projeto
---
O Coletivo de Estudos sobre a Revolução Cubana surgiu do encontro de pesquisadores / 
doutorandos cujos projetos foram conformados por distintas dimensões da revolução Cubana,
e cuja militância priorizou aprofundamento, acompanhamento contínuo e formação ampla
para todos os interessados ao máximo possível.
Tal encontro foi possível graças à rede formada, entre outras universidades, pelo 
Programa de Pós-Graduação em Política Social da  UFES e o programa similar da PUC-RS.
Formada em torno de projetos de pesquisa internacionais e multidisciplinares, a rede
permitiu estâncias em Cuba, e expansão da rede com a participação de professoras e 
pesquisadoras cubanas.
O grupo formou-se, concretamente, em 2020, com a proposta de oferecer um primeiro curso de extensão
sobre a Revolução Cubana, aproveitando a experiência das distintas teses finalizadas ou em andamento.


**Nossos Objetivos**
- Entender a revolução cubana em movimento a partir de uma abordagem materialista e dialética.
- Compreender a reprodução social articulada em sua totalidade
- Apreender as determinações e contradições da Revolução Cubana no âmbito da luta de classes global
- Estudar a transição ao socialismo cubana como processo imerso na luta de classes
- Aprofundar o acompanhamento e conhecimento sobre a Revolução Cubana ao máximo no Brasil.
- Construir conhecimento vinculado às lutas locais a partir dos ensinamentos da transição em Cuba.

