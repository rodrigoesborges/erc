# IAGML Website

[![build status](https://travis-ci.org/iagml/website.svg?branch=master)](https://travis-ci.org/iagml/website)
![last commit](https://img.shields.io/github/last-commit/iagml/iagml.github.io)

[![powered by Github Pages](https://img.shields.io/badge/powered%20by-Github%20Pages-171717?logo=github)](https://pages.github.com)
[![powered by Jekyll](https://img.shields.io/badge/powered%20by-Jekyll-E0115F?logo=jekyll)](https://jekyllrb.com)
[![made with Bootstrap](https://img.shields.io/badge/made%20with-bootstrap-602c50?logo=bootstrap)](https://getbootstrap.com)
![licence](https://img.shields.io/github/license/iagml/iagml.github.io)

## Sobre

Repositório do site do Coletivo de Estudos sobre a Revolução Cubana.
Acesse o site neste link: [erc.grupo.pro.br](https://erc.grupo.pro.br)

## License

The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
