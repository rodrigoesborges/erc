---
title: "Curso -  Revolução Cubana: 60 anos depois"
date: 2020-09-04T17:06:45.674Z
image: /assets/uploads/curso01cartaz.jpg
author: alinefae
tags:
  - Cursos
  - Extensão
  - Conjuntura
  - História da Revolução Cubana
  - Desafios
---
*Curso de Extensão Revolução Cubana: 60 anos depois*

- *Carga Horária*: 21 horas

- *Período*: de 20 de outubro de 2020 a 01 de dezembro de 2020

- *Horário*: das 18h30m às 21h30m

- *Inscrições*: de 05 a 15 de outubro de 2020 _(via formulário google)_


- *Ementa*:
Em 1959, há exatos 61 anos, Cuba iniciou um processo revolucionário que segue promovendo mudanças profundas em sua sociedade. 
A Revolução, que nasceu “para os humildes”, se forjou socialista em 1961, em meio ao processo de expropriação e desapropriação de meios de produção e terras e de intensa luta de classes dentro do país e com o exterior. 
Desde o início, Cuba enfrenta o bloqueio econômico dos Estados Unidos (EUA) e, sob condições histórico-concretas particularmente difíceis, tem adotado uma política econômica e social planificada, articulada ao desenvolvimento do poder popular, com o objetivo de responder às necessidades sociais de sua população. 
A partir de 1990, uma grave crise econômica ensejou um novo ciclo de mudanças, que vem se consolidando em um processo chamado _Atualização do Modelo Econômico e Social da Revolução_.
Com o intuito de garantir os avanços de sua revolução, as mudanças recentes também imputam novas contradições à sociedade cubana. 
Assim, este curso *objetiva compreender as particularidades que caracterizam a Revolução Cubana*, seus aspectos históricos, seus avanços, suas contradições e, sobretudo, *conhecer mais dessa experiência de transição ao socialismo*, considerando toda a complexidade que envolve a teoria e prática quanto construção de uma nova sociabilidade.



